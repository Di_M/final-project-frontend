import axios from "axios";
import LocalStorage from "../LocalStorageRequest";
import { routes } from '../../constants/constRouters';

export const getAuthToken = () => {
    let authToken = LocalStorage.getData("token");

    if (!authToken) {
        return "";
    }

    return `JWT ${authToken}`;
};

export const getApiRequestConfig = () => ({
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: getAuthToken(),
    },
    withCredentials: false,
});

// temporary Request from Public Folder CourseData

export const getFakeCourseData =()=>{
    axios.get(`${process.env.PUBLIC_URL}/MathElementaryCourseData.json`).then(response=>(response.data))
}


export const axiosInstance = axios.create();

axiosInstance.interceptors.response.use(
        (response) => response,
        (error) => {
            if (error.response.status === 401 || error.response.status === 403) {
                window.location = `${routes.signIn.href}`
            }
            return Promise.reject(error);
        }
    )
