import { useState, useEffect } from 'react';
import {useDispatch, useSelector} from "react-redux";
import convertLessonId from "../helpers/ConvertLessonId";
import * as LessonActions from "../../actions/LessonActions";

function useLessonsUpdate() {
    const dispatch = useDispatch();
    const getCourseDetails  = useSelector(state => state.CourseDetailsReducer);
    const courseData = getCourseDetails.courseDetails;
    console.log('Layout=>AppRoute.js, current Course Data from CourseDetailsReducer:', courseData);
    // Create a list of Lessons Id

    let userIdList = convertLessonId(courseData);
    const updateLessonListStorage =()=>{
        dispatch(LessonActions.addLessonIdList(userIdList));
    };
    if (userIdList !==undefined){updateLessonListStorage()}
    return ( console.log('ID Updated'))
}

export default useLessonsUpdate