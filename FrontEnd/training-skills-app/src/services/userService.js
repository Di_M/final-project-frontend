import GeneralApi from "./generalApi";
import LocalStorage from "./LocalStorageRequest";


function login (opt) {
    const { name, password, role } = opt;

    return GeneralApi.sendUserLogIn({name, password, role})
        .then(({data: response}) => {

            LocalStorage.setData('token', response.jwtToken);
            LocalStorage.setData('userID', response.userId);
            LocalStorage.setData('role', role);
            return { logged: true, userId: response.userId}
        })
        .catch((error) => {
            if (error.response && error.response.data.message) {
                return { logged: false, error: error.response.data.message };
            }
            // eslint-disable-next-line no-console
            console.log("Fetch error: ", error);

            return { logged: false};
        });
}

function isLoggedIn() {
    return !!LocalStorage.getData('userID');
}

async function regUser (opt) {
    const { name, email, password } = opt;

    return await GeneralApi.sendUserReg({name, email, password})
        .then(({data: response}) => {
            console.log(response);
            return { registered: true };
        })
        .catch((error) => {
            if (error.response && error.response.data.message) {
                return { logged: false, error: error.response.data.message };
            }
            // eslint-disable-next-line no-console
            console.log("Fetch error: ", error);

            return { registered: false};
        });
// //Sing Up
// export const postParent = async (url, data) => {

//     try {
//         let response = await axios.post(url, data);
//         console.log("Ressponse--->", response.status);
//         return response;

//     } catch(error) {

//         if (error.response) {
//             console.log(error.response.data);
//             console.log(error.response.status);
//             console.log(error.response.headers);
//             return error.response
//         }
//         console.log(error);
//     }
// };
}

async function changePassword (opt) {
    const { oldPassword, password } = opt;

    return await GeneralApi.putParentPassword({password, oldPassword})
        .then(({data: response}) => {
            console.log(response);
            return {changed: true, message: response.message}
        })
        .catch((error) => {
            if (error.response && error.response.data.message) {
                return { changed: false, error: error.response.data.message };
            }
            // eslint-disable-next-line no-console
            console.log("Fetch error: ", error);

            return {changed: false}
        });
}

const userService = {
    login,
    isLoggedIn,
    regUser,
    changePassword
};

export default userService;
