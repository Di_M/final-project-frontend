export const lessonFakeData =
{
    lessonId: 1,
        lessonPoints: 2,
    lessonImg: 'https://i.ibb.co/GpdtL3w/Error.png',
    lessonType: 'counting',
    title: "Testing Lesson Data. Check Validity of Props",
    description: "This Lesson was used as a test Example. Please write our support if you see this data",
    exampleBody:[
    {  id:1,
        imgArray:['https://i.ibb.co/GpdtL3w/Error.png','https://i.ibb.co/GpdtL3w/Error.png'],
        imgNumber:1,
        displayAnswer:1,
        description:'One ERROR',
        symbol:'='
    },
    {  id:2,
        imgArray:['https://i.ibb.co/GpdtL3w/Error.png'],
        imgNumber:2,
        displayAnswer:2,
        description:'Two ERRORS',
        symbol:'='
    },
    {  id:3,
        imgArray:['https://i.ibb.co/GpdtL3w/Error.png'],
        imgNumber:3,
        displayAnswer:3,
        description:'Three ERRORS',
        symbol:'='
    }

],
    questionTitle: "Don't Sovle this Question- ask help from our support",
    questionType:'counting',
    questionBody: [
    {  id:1,
        imgArray:['https://i.ibb.co/GpdtL3w/Error.png'],
        imgNumber:2,
        displayAnswer:'',
        description:'Count errors and enter a number:',
        symbol:'',
        question:true
    }],
    questionAnswer:{
    type:'counting',
        rightAnswer: 2,
        rightMessage: 'Answer is right! ONLY 2 ERRORS',
        IsAnswered: false
},

    lessonIsFinished: false
};
