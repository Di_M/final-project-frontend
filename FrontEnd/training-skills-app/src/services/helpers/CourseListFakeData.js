export const courseListFakeData= [{
    _id: "5edfccb7393cf83e30f77f12",
    courseImg: "https://i.ibb.co/2Kfp3v9/teacher-maths-female-icon.png",
    courseTitle: "Elementary Math for Children",
    courseDescription: "You will learn how to count to 20, elementary math operations, logic tests.",
    courseSubject: "Math",
    courseGrade: "Elementary"
}]