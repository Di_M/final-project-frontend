import request from "../request";
import { url } from "../../constants/constURL";
const {
    axiosInstance,
    getApiRequestConfig
} = request;


// export const getOneLesson = (idLesson) =>
//     axiosInstance
//         .get((`${url.oneLessonView}${idLesson}`), getApiRequestConfig());

export const getOneLesson = (lessonId)=>{
    axiosInstance.get((`${url.oneLessonView}/${lessonId}`), getApiRequestConfig())
}

export const postLessonAnswer = (idLesson, childAnswer) =>
    axiosInstance
        .post((`${url.oneLessonAnswer}${idLesson}`), {
            childAnswer
        }, getApiRequestConfig());