import * as types from '../constants/constTypes';

const initState = {
    token: null,
    auth: false
};

function TokenReducer(state = initState, action) {

    switch (action.type) {

        case types.GET_TOKEN:
            return {
                ...state,
                ...{
                    token: action.payload,
                    auth: true
                }
            };
        case types.DELETE_TOKEN:
            return {
                ...initState
            };

        default:
            return state
    }
}

export default (TokenReducer);