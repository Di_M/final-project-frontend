import { combineReducers } from 'redux'
import TokenReducer from './TokenReducer';
import UserIDReducer from './UserIDReducer';
import CreditCardReducer from './CreditCardReducer';
import MyChildrenReducer from "./MyChildrenReducer";
import AvatarReducer from "./AvatarReducer";
import RoleReducer from "./RoleReducer";
import CourseListReducer from "./CourseListReducer"
import CourseDetailsReducer from "./CourseDetailsReducer"
import LessonIdReducer from "./lessonReducer"


export default combineReducers({
    TokenReducer,
    UserIDReducer,
    CreditCardReducer,
    MyChildrenReducer,
    AvatarReducer,
    RoleReducer,
    CourseListReducer,
    LessonIdReducer,
    CourseDetailsReducer,

})

