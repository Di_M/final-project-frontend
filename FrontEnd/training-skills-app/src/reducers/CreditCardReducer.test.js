import * as types from '../constants/constTypes';
import CreditCardReducer from './CreditCardReducer';

describe('Credit Card Reducer', () => {

    describe('Test Initial state', () => {

        it('Should render initial state', () => {
            const newState = CreditCardReducer(undefined, {});
            expect(newState).toEqual({
                creditCard: {},
                added: false
            });
        });

    });

    describe('Test New state', () => {

        let someCardInfo;

        beforeEach(() => {
            someCardInfo = {
                name: 'My card',
                number: 1212,
                cardHolder: 'Dima'
            };
        });

        it('Should return new state if receiving type GET_CREDIT_CARD', () => {

            const newState = CreditCardReducer(undefined, {
                type: types.GET_CREDIT_CARD,
                payload: someCardInfo
            });
            expect(newState).toEqual({
                creditCard: someCardInfo,
                added: true
            });
        });

        it('Should return initState if receiving type DELETE_TOKEN', () => {

            CreditCardReducer(undefined, {
                type: types.GET_CREDIT_CARD,
                payload: someCardInfo
            });
            const finallyState = CreditCardReducer(undefined, {
                type: types.DELETE_CREDIT_CARD,
            });
            expect(finallyState).toEqual({
                creditCard: {},
                added: false
            });
        });

    });




});