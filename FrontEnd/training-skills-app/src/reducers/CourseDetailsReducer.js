import * as types from '../constants/constTypes';
import fakeCourseData from '../services/helpers/CourseFakeData'

const initState = {
    courseDetails: fakeCourseData,
    uploaded: false
};


function CourseDetailsReducer (state = initState, action) {
    switch (action.type) {

        case types.GET_COURSE_DETAILS:
            return {
                ...state,
                ...{
                    courseDetails:action.payload.preparedData,
                    uploaded: action.payload.uploaded
                }
            };
            // return Object.assign( {}, state, {courseDetails:action.payload.preparedData,
            //     uploaded: action.payload.uploaded})

        case types.CLEAR_COURSE_DETAILS:
            return {
                ...initState,
            };

        default:
            return initState;

    }
}
export default CourseDetailsReducer
