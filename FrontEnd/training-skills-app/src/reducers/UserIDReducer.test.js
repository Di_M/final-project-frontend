import * as types from '../constants/constTypes';
import UserIDReducer from './UserIDReducer';

describe('UserID Reducer', () => {

    describe('Test Initial state', () => {

        it('Should render initial state', () => {
            const newState = UserIDReducer(undefined, {});
            expect(newState).toEqual({
                userID: null
            });
        });

    });

    describe('Test New state', () => {

        let someId;

        beforeEach(() => {
            someId = 'sd234dsfsd23';
        });

        it('Should return new state if receiving type GET_USER_ID', () => {
            const newState = UserIDReducer(undefined, {
                type: types.GET_USER_ID,
                payload: someId
            });
            expect(newState).toEqual({
                userID: someId
            });
        });

        it('Should return initState if receiving type DELETE_USER_ID', () => {
            UserIDReducer(undefined, {
                type: types.GET_USER_ID,
                payload: someId
            });
            const finallyState = UserIDReducer(undefined, {
                type: types.DELETE_USER_ID,
            });
            expect(finallyState).toEqual({
                userID: null
            });
        });

    });


});