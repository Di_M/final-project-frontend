import * as types from '../constants/constTypes';
import {courseListFakeData} from '../services/helpers/CourseListFakeData'

const initState = {
    courseList: courseListFakeData,
    uploaded: false,
    type:'Used Local Data'
};

function CourseListReducer (state = initState, action) {

    switch (action.type) {

      case types.SHOW_COURSE_LIST:
            return {
                ...state,
                ...{
                    courseList: action.payload,
                    uploaded: true,
                    type:'Used Server Data'
                }
            };

        case types.CLEAR_COURSE_LIST:
            return {
                ...initState,
            };

        default:
            return initState;

    }
}
export default CourseListReducer
