import * as types from '../constants/constTypes';

export function getToken(list) {

    return dispatch => {
        dispatch({
            type: types.GET_TOKEN,
            payload: list
        });
    };
}

export function deleteToken() {

    return dispatch => {
        dispatch({
            type: types.DELETE_TOKEN
        });
    };
}