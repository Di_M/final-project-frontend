import * as types from '../constants/constTypes';

export function getUserID(list) {

    return dispatch => {
        dispatch({
            type: types.GET_USER_ID,
            payload: list
        });
    };
}

export function deleteUserID() {

    return dispatch => {
        dispatch({
            type: types.DELETE_USER_ID,
        });
    };
}