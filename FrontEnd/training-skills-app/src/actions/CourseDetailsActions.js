import React from 'react'
import * as types from '../constants/constTypes';



export const addCourseDetails = (courseDetails) => {
    const preparedData = courseDetails[0]

        return dispatch => {
            dispatch({
                type: types.GET_COURSE_DETAILS,
                payload: {
                    preparedData,
                    uploaded:true
                }
            });
        };


}



export function clearCourseDetails() {

    return dispatch => {
        dispatch({
            type: types.CLEAR_COURSE_DETAILS,
        });
    };
}