const deployURL = 'https://elem-school-server.herokuapp.com/';
// const deployURL = 'http://localhost:4004/';

export const url = {

    signUP: `${deployURL}sign-up`,
    signIn: `${deployURL}sign-in`,
    changePassword: `${deployURL}change-password`,
    signUpChild: `${deployURL}child/add`,
    deleteChild: `${deployURL}child/`,
    editChild: `${deployURL}child/edit/`,
    assignCourse: `${deployURL}child/add-course`,
    reassignCourse: `${deployURL}child/remove-course`,
    userInfo: `${deployURL}account`,
    coursesList: `${deployURL}course/list`,
    fullCoursesList: `${deployURL}course/list/details`,
    oneCourse: `${deployURL}course/view/`,
    oneLessonView: `${deployURL}lesson/view/`,
    oneLessonAnswer: `${deployURL}lesson//answer/`,
    coursesDetails: `${deployURL}course/list/details`
};
