export const routes = {
    home: {name: 'Home', href: '/'},
    about:{name:'About', href: '/about'},
    contacts:{name:'Contacts', href:'/contacts'},
    faq:{name:'FAQ', href:'/faq'},
    signUp: {name: 'Sign Up', href: '/sign-up'},
    signUpParent: {name: 'Parent', href: '/sign-up/parent'},
    signIn: {name: 'Sign In', href: '/sign-in'},
    reestablishPassword: {name: 'Reestablish Password', href: '/reestablish'},
};