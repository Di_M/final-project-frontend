import React from 'react';
import { useSelector } from "react-redux";
import Routes from "./Routes";
import Authentication from "../services/helpers/Authentication";

function App() {

    const auth = Authentication();
    const userId = useSelector(state => state.UserIDReducer.userID);
    const role = useSelector(state => state.RoleReducer.role);

    return (
        <div className="app">
            <Routes auth={auth} role={role} id ={userId} />
        </div>
    );

}

export default App;