import React from 'react';
import './Footer.less'
import logoW from './images/logoW.png'

const Footer =props=>{

    return <React.Fragment>
        <footer className="footer-section">
            <div className="footer-section-container">
            <div className="footer-logo">
                <img alt="logo"  src={logoW}/>
            </div>
            <div className="footer-links">
                <a href="#">Privacy Policy</a>
                <a href="#">Terms of Use</a>
                <a href="#">Contacts</a>
            </div>
            </div>

        </footer>
    </React.Fragment>
}
export default Footer