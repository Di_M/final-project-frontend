import React from 'react'
import './HomePagePrograms.less'
import  english_course from './images/english_course.jpg'
import logic_course from './images/logic_course.jpg'
import math_course from './images/math_course.jpg'

const HomePagePrograms = props=>{

    return ( <React.Fragment>
        <section className="our-programs">
            <h2 className="our-programs-title">Choose from the next programs</h2>
            <div className="our-programs-section">
                <div className="our-programs-section-main our-programs-section-preschool">
                    <h3 className="our-programs-section-title">Preschool</h3>
                    <div className="our-programs-section-courses">
                            <a href="#" className="our-programs-section-course-item">
                            <img src={english_course} alt="english-course"/>
                            <div  className="our-programs-section-course-item-description">English</div>
                            <div  className="our-programs-section-course-item-grade">Elementary</div>
                            </a>
                        <a href="#" className="our-programs-section-course-item">
                            <img src={logic_course} alt="logic-course"/>
                            <div  className="our-programs-section-course-item-description">Logic</div>
                            <div  className="our-programs-section-course-item-grade">Elementary</div>
                        </a>
                        <a href="#" className="our-programs-section-course-item">
                            <img src={math_course} alt="math-course"/>
                            <div  className="our-programs-section-course-item-description">Math</div>
                            <div  className="our-programs-section-course-item-grade">Elementary</div>
                        </a>
                    </div>
                    </div>
                <div className="our-programs-section-main our-programs-section-school">
                    <h3 className="our-programs-section-title title-school">School</h3>
                    <div className="our-programs-section-courses">

                        <a href="#" className="our-programs-section-course-item">
                            <img src={math_course} alt="english-course"/>
                            <div  className="our-programs-section-course-item-description">Math</div>
                            <div  className="our-programs-section-course-item-grade item-grade-basic">Basic</div>
                        </a>

                        <a href="#" className="our-programs-section-course-item">
                            <img src={english_course} alt="english-course"/>
                            <div  className="our-programs-section-course-item-description">English</div>
                            <div  className="our-programs-section-course-item-grade item-grade-basic">Basic</div>
                        </a>

                    </div>
                </div>
            </div>

        </section>
    </React.Fragment>)
}
export default HomePagePrograms;