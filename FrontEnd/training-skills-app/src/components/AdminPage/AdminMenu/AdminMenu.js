import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { Layout, Menu, Avatar, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import AvatarModal from "../../lib/Avatar/AvatarModal";

import './AdminMenu.less';
import {useSelector} from "react-redux";
import CaretUpOutlined from "@ant-design/icons/lib/icons/CaretUpOutlined";

const AdminMenu = (props) => {

    const { id, menuList, path } = props;

    const avatarData = useSelector(store => store.AvatarReducer);

    const [collapsed, setCollapsed] = useState(false);
    const [collapsible, setCollapsible] = useState(true);

  const [avatarModal, setAvatarModal] = useState(false);

    const onChangeCollapse = () => {
        setCollapsed(!collapsed);
    };

    const showAvatarModal = () => {
      setAvatarModal(!avatarModal);
    };

    const menuItems = menuList.map((item, i) => (
        <Menu.Item key={`/account/${id}${item.linkName}`} icon={item.icon}>
            <span>{item.title}</span>
            <Link className="admin-sider-link" to={`/account/${id}${item.linkName}`} />
        </Menu.Item>
        ));

    return (
        <Layout.Sider
            collapsible={collapsible}
            breakpoint="md"
            collapsed={collapsed}
            onCollapse={onChangeCollapse}
            onBreakpoint={broken => {
                setCollapsible(!broken);
            }}
        >
          <AvatarModal
            visible={avatarModal}
            showAvatarModal={showAvatarModal}
            handleCancel={() => showAvatarModal()}
          />
            <div className="admin-menu-logo">
                <Avatar size={(collapsed) ? 40 : 84} icon={<UserOutlined />} src={avatarData.avatar}/>
                <Button
                  className="admin-menu-add-avatar-btn"
                  onClick={() => showAvatarModal()}>{(collapsed) ? <CaretUpOutlined /> : 'Add Avatar'}</Button>
            </div>
            <Menu theme="dark" defaultSelectedKeys={[path]} mode="inline">
                {menuItems}
            </Menu>
        </Layout.Sider>
    );
};

export default AdminMenu;
