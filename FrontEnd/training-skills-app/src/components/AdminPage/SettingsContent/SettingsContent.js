import React from 'react';
import './SettingsContent.less';
import FormChangePassword from '../../lib/DataEntry/FormChangePassword';

const SettingsContent = () => {


    return (

        <div className="settings-dashboard">
            <h2 className="settings-dashboard-title">
                Settings
            </h2>

            <div className="settings-dashboard-form-change-password">
                <FormChangePassword />
            </div>
        </div>
    );
};

export default SettingsContent;