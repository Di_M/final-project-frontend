import React from 'react';
import { Avatar } from 'antd';
import './ChildrenNameTitle.less';

const ChildrenNameTitle = (props) => {

    const {childrenName, gender, avatarSRC} = props;

    const defaultAvatar = gender === 'female' ? 'https://ih1.redbubble.net/image.120565724.7743/st,small,507x507-pad,600x600,f8f8f8.u2.jpg' : 'https://f0.pngfuel.com/png/1/197/child-computer-icons-portfolio-school-little-boy-png-clip-art.png';

    return (
        <div className="children-name-block">

            <Avatar className="children-name-avatar" size={64} src={ avatarSRC || defaultAvatar } />

            <p className="children-name-title">
                {childrenName || "Children name"}
            </p>
        </div>
    );
};

export default ChildrenNameTitle;
