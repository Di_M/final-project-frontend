import React from "react";
import {NavLink} from "react-router-dom";
import {Collapse, Statistic} from "antd";
import {CheckCircleOutlined, ForwardOutlined, ScheduleOutlined, ThunderboltOutlined} from "@ant-design/icons";
import './ChildrenCourseResultItem.less';

const { Panel } = Collapse;

const ChildrenCourseResultItem = (props) => {

    const {titleCourse, levelCourse, finishedLessons, quizScore, points} = props;


    // See more link. Have to transfer to Course
    const extraPanelItem = () => (
        <NavLink
            to={'/account/:id/courses'}
            className='children-courses-result-link'
        >
            See more
        </NavLink>
    );

    return (
        <Collapse
            className="children-courses-result-item"
            bordered={false}
            expandIcon={({ isActive }) => <ForwardOutlined rotate={isActive ? 90 : 0} />}
        >
            <Panel className="children-courses-result-item-panel"
                   header={titleCourse + '  ' + levelCourse}
                   key="1"
                   extra={extraPanelItem()}
            >
                <div className="children-courses-result-item-data">
                    <div>
                        <Statistic
                            title="Finished lessons:"
                            value={finishedLessons || 0}
                            prefix={<ScheduleOutlined style={{ fontSize: '30px', color: '#08c' }}/>}
                        />
                    </div>
                    <div>
                        <Statistic
                            title="Quiz score:"
                            value={quizScore || 0.00}
                            precision={2}
                            prefix={<ThunderboltOutlined style={{ fontSize: '30px', color: '#ba8b00' }}/>}
                            suffix="%"
                        />
                    </div>
                    <div>
                        <Statistic
                            title="Points:"
                            value={points || 0}
                            prefix={<CheckCircleOutlined style={{ fontSize: '30px', color: '#52c41a' }}/>}
                        />
                    </div>
                </div>
            </Panel>
        </Collapse>
    );
};

export default ChildrenCourseResultItem;