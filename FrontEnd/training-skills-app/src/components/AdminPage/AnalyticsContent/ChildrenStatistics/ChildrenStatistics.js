import React from 'react';
import './ChildrenStatistics.less';
import ChildrenTotalStatistics from '../../../lib/DataDisplay/ChildrenTotalStatistics';
import ChildrenNameTitle from '../ChildrenNameTitle/ChildrenNameTitle';
import ChildrenCourseResult from '../ChildrenCourseResult/ChildrenCourseResult';


const ChildrenStatistics = (props) => {

    const {childrenName, gender, avatar, totalScoreLesson, totalQuizPercent, totalPoints, coursesList, role} = props;

    return (
        <div className="children-statistics">

            {(role === "parent")
                ?
                <ChildrenNameTitle
                    childrenName={childrenName}
                    avatarSRC={avatar}
                    gender={gender}
                />
                :
                ''
            }

            <ChildrenTotalStatistics
                totalScoreLesson={totalScoreLesson}
                totalQuizPercent={totalQuizPercent}
                totalPoints={totalPoints}
            />

            {(coursesList)
                ?
                <ChildrenCourseResult coursesList={coursesList}
                />
                :
                ''
            }
        </div>
    );

};

export default ChildrenStatistics;
