import React, {useState} from 'react';
import {useSelector} from "react-redux";
import { Select } from 'antd';

import ChildrenStatistics from './ChildrenStatistics/ChildrenStatistics';
import ChildrenFilter from '../../lib/DataEntry/ChildrenFilter';

import './AnalyticsContent.less';

const { Option } = Select;


const AnalyticsContent = () => {

    let children = useSelector(store => store.MyChildrenReducer);
    const role = useSelector(store => store.RoleReducer.role);

    console.log(children);

    let statisticList = null;
    let selectOptions = null;


    const [listChildren, setListChildren] = useState(children);


    if (!listChildren) {
        statisticList = <p className="analytics-content-empty-block">The list is empty. Add a child to fill it:)</p>;
    } else {
        statisticList = listChildren.map((item, i) => (
            <ChildrenStatistics
                key={i}
                gender={item.gender}
                childrenName={item.name}
                avatar={item.avatar}
                totalScoreLesson={item.totalScoreLesson}
                totalQuizPercent={item.totalQuizPercent}
                totalPoints={item.totalPoints}
                coursesList={item.courses}
                role={role}
            />
        ));
    }

    if(children.length >= 2) {
        selectOptions = children.map((item,i) => (
            <Option key={i} value={item.name}>{item.name}</Option>
        ));
    }


    function handleChange(value) {
        if(value === 'all') {
            setListChildren(children);
        } else {
            const newList = children.filter(item => (item.name === value));
            setListChildren(newList);
        }
    }

    console.log('Stat -->', statisticList.length);
    return (
        <div className="analytics-content">
            <h2 className="analytics-content-title">
                Analytics
            </h2>

            {
                (selectOptions) &&

                 <ChildrenFilter
                     title={'Children'}
                     onChange={handleChange}
                     options={selectOptions}
                 />
            }

            {statisticList}

        </div>
    );
};

export default AnalyticsContent;
