import React, {useState, useEffect} from 'react';
import './AwardsChildrenList.less';
import ChildrenNameTitle from "../../AnalyticsContent/ChildrenNameTitle/ChildrenNameTitle";
import AwardsListItems from '../AwardsListItems/AwardsListItems';


const AwardsChildrenList = (props) => {

    const {childrenName, gender, avatar, role, coursesList} = props;

    const [firstCourseDone, setFirstCourseDone] = useState(false);
    const [mathCourseDone, setMathCourseDone] = useState(false);
    const [englishCourseDone, setEnglishCourseDone] = useState(false);
    const [allCoursesDone, setAllCoursesDone] = useState(false);

    useEffect(() => {
        let firstCourseDoneItem = coursesList.find(item => item.courseIsFinished === false);

        if (firstCourseDoneItem && firstCourseDoneItem.length === 1) {
            setFirstCourseDone(true);
        }
    });

    return (
        <div className="awards-children-list">
            {(role === "parent")
                ?
                <ChildrenNameTitle
                    childrenName={childrenName}
                    avatarSRC={avatar}
                    gender={gender}
                />
                :
                ''
            }

            <AwardsListItems
                firstCourseDone={firstCourseDone}
                mathDone={mathCourseDone}
                englishDone={englishCourseDone}
                allCoursesDone={allCoursesDone}
            />

        </div>
    );

};

export default AwardsChildrenList;