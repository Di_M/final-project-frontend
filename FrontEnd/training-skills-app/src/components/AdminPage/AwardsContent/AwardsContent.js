import React, {useState} from 'react';
import {useSelector} from "react-redux";
import { Select } from 'antd';
import './AwardsContent.less';
import ChildrenFilter from "../../lib/DataEntry/ChildrenFilter";
import AwardsChildrenList from "./AwardsChildrenList/AwardsChildrenList";

const { Option } = Select;

const AwardsContent = () => {

    let children = useSelector(store => store.MyChildrenReducer);
    const role = useSelector(store => store.RoleReducer.role);

    let awardsList = null;
    let selectOptions = null;


    const [listChildren, setListChildren] = useState(children);

    if (!listChildren) {
        awardsList = <p className="awards-content-no-awards">The list is empty. Add a child to fill it:)</p>;
    } else {
        awardsList = listChildren.map((item, i) => (
            <AwardsChildrenList
                key={i}
                gender={item.gender}
                childrenName={item.name}
                avatar={item.avatar}
                role={role}
                coursesList={item.courses}
            />
        ));
    }

    if(children.length >= 2) {
        selectOptions = children.map((item,i) => (
            <Option key={i} value={item.name}>{item.name}</Option>
        ));
    }


    function handleChange(value) {
        if(value === 'all') {
            setListChildren(children);
        } else {
            const newList = children.filter(item => (item.name === value));
            setListChildren(newList);
        }
    }


    return (
        <div className="awards-content">
            <h2 className="awards-content-title">
                Awards
            </h2>

            {
                (selectOptions) &&

                <ChildrenFilter
                    title={'Children'}
                    onChange={handleChange}
                    options={selectOptions}
                />
            }

            {awardsList}

        </div>
    );
};

export default AwardsContent;
