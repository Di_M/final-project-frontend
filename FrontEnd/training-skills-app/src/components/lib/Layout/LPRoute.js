import React from "react";
import { Route, Redirect } from "react-router-dom";

import MainNavigation from "../Navigation/MainNavigation"
import Footer from "../../../components/HomePage/Footer/Footer"

export default LPRoute;

function LPRoute({
    render,
    component,
    auth,
    userId,
    role,
    ...routeProps
}) {

    const renderLayout = () => (
        <>
            <MainNavigation/>
                <main>
                    {render 
                        ? render()
                        : React.createElement(component)}
                </main>
            <Footer/>
        </>
    );
    
    return (
        <>
            {
                !auth 
                ? <Route 
                    {...routeProps} 
                    render={() => renderLayout()}
                    />
                : <Redirect to={`/account/${userId}`} />
                //     (role !== 'parent' 
                //     ? <Redirect to={`/account/${userId}/courses`} />  
                //     : <Redirect to={`/account/${userId}`} />
                //     )
                // )
            }
        </>
    );
};
