import React from "react";
import { useHistory } from 'react-router-dom'
import {PageHeader} from "antd";
import './AuthHeader.less';

const AuthHeader = () => {

    const history = useHistory();

    return (
        <PageHeader
            className="auth-page-header"
            onBack={() => history.goBack()}
            title={`...`}
        />
    );
};

export default AuthHeader;