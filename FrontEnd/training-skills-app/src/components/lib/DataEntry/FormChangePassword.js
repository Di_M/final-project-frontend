import React from 'react';
import {
    Form,
    Input,
    Button, Tooltip, message,
} from 'antd';
import './FormChangePassword.less';
import {InfoCircleOutlined} from "@ant-design/icons";
import userService from "../../../services/userService";


const FormChangePassword = () => {

    const [form] = Form.useForm();

    const layout = {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 },
    };

    const tailLayout = {
        wrapperCol: { offset: 0, span: 24 },
    };

    const onFinish = async (values) => {
        console.log('Received values of form: ', values);

        userService.changePassword(values).then((result) => {

            console.log(result);

            if (result.error) {
                console.log(result.error);
                message.error(`${result.error}`);
            } else {
                console.log('Result -->', result);

                message.success(`${result.message}`);
                form.resetFields();
            }
        });
    };


    return (

        <div className="settings-dashboard-form-change-password-form">

            <div className="settings-dashboard-form-change-password-title">
                <span>Change your password </span>
                <Tooltip title="There are you can change your password">
                    <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                </Tooltip>
            </div>

            <Form
                form={form}
                {...layout}
                className="changePassword-form"
                name="changePassword"
                onFinish={onFinish}
                scrollToFirstError
            >

                <Form.Item
                    name="oldPassword"
                    label="Old password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="password"
                    label="New Password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="confirm"
                    label="Confirm new password"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject('The two passwords that you entered do not match!');
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>


                <Form.Item {...tailLayout}>
                    <Button className="change-password-btn" type="primary" htmlType="submit">
                        Change Password
                    </Button>
                </Form.Item>

            </Form>
        </div>
    );
};

export default FormChangePassword;