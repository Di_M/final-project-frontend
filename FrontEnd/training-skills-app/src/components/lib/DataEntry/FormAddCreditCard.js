import React, {useEffect} from 'react';
import {Form, Input, InputNumber} from "antd";
import './FormAddCreditCard.less';

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    }
};


const FormAddCreditCard = (props) => {

    const {formCreditCard, dataValues, modalVisibility} = props;

  useEffect(() => {
    if (dataValues) {
      formCreditCard.setFieldsValue({
        cardName: dataValues.cardName,
        cardNumFirst: dataValues.cardNumFirst,
        cardNumSecond: dataValues.cardNumSecond,
        cardNumThird: dataValues.cardNumThird,
        cardNumLast: dataValues.cardNumLast,
        nameHolder: dataValues.nameHolder,
        cardMonth: dataValues.cardMonth,
        cardYear: dataValues.cardYear
      });
    }
  }, [modalVisibility]);




    return (
        <Form
            {...formItemLayout}
            form={formCreditCard}
            name="formAddCard"
        >

            <Form.Item
                label="Card name"
                name="cardName"
                rules={[{ required: true, message: 'Please input your card name!' }]}
            >
                <Input allowClear/>
            </Form.Item>


            <Form.Item
                label="Card number"
            >
                <Form.Item
                    name={'cardNumFirst'}
                    noStyle
                    rules={[{ required: true, message: 'Please input first 4 numbers of your card!' }]}
                >
                    <InputNumber
                        style={{width: 85}}
                        min={1000}
                        max={9999}
                        step={1}
                    />
                </Form.Item>

                <Form.Item
                    name={'cardNumSecond'}
                    noStyle
                    rules={[{ required: true, message: 'Please input second 4 numbers of your card!' }]}
                >
                    <InputNumber
                        style={{width: 85}}
                        min={1000}
                        max={9999}
                        step={1}
                    />
                </Form.Item>

                <Form.Item
                    name={'cardNumThird'}
                    noStyle
                    rules={[{ required: true, message: 'Please input third 4 numbers of your card!' }]}
                >
                    <InputNumber
                        style={{width: 85}}
                        min={1000}
                        max={9999}
                        step={1}
                    />
                </Form.Item>

                <Form.Item
                    name={'cardNumLast'}
                    noStyle
                    rules={[{ required: true, message: 'Please input last 4 numbers of your card!' }]}
                >
                    <InputNumber
                        style={{width: 85}}
                        min={1000}
                        max={9999}
                        step={1}
                    />
                </Form.Item>

            </Form.Item>


            <Form.Item
                label="Holder name"
                name="nameHolder"
                rules={[{ required: true, message: 'Please input holder name!' }]}
            >
                <Input allowClear />
            </Form.Item>


            <Form.Item
                label="Card period"
            >
                <Form.Item
                    name={'cardMonth'}
                    noStyle
                    rules={[{ required: true, message: 'Please input month!' }]}
                >
                    <InputNumber
                        min={1}
                        max={12}
                        step={1}
                        // formatter={value => {
                        //     if (value.length < 2 && value !== '') {
                        //         return `0${value}`;
                        //     }
                        //     return value;
                        // }}
                    />
                </Form.Item>

                <span>{" / "}</span>

                <Form.Item
                    name={'cardYear'}
                    noStyle
                    rules={[{ required: true, message: 'Please input year!' }]}
                >
                    <InputNumber
                        min={20}
                        max={30}
                        step={1}
                    />
                </Form.Item>
            </Form.Item>

        </Form>
    );
};

export default FormAddCreditCard;
