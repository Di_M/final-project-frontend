import React from 'react';
import {Button} from "antd";
import './SignOutButton.less';

const SignOutButton = (props) => {

    const {title, color, onClick} = props;

    return(
        <Button
            className={"logout-btn " + color}
            type="primary"
            size="large"
            onClick={onClick}
        >
            {title}
        </Button>
    );

};

export default SignOutButton;