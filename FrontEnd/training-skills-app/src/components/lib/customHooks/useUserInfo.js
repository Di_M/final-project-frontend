import {useState, useEffect} from "react";
import LocalStorage from "../../../services/LocalStorageRequest";
import GeneralApi from "../../../services/generalApi";

// The purpose of this hook was created to fix request order issue. It should be applied in AdminPage.
// To be decided upon that later...
export const useUserInfo = () => {
  const [userInfo, setUserInfo] = useState(null);

  const request = async () => {
    const userId = LocalStorage.getData('userID');
    const userData = await GeneralApi.getUserInfo(userId); //
    console.log('UserData:', userData);
    setUserInfo(userData);
  };

  useEffect(() => {
    (async () => {
      await request()
    })();
  }, []);

  return {
    userInfo
  }

};

