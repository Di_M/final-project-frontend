import React from 'react';
import {Button} from "antd";
import './ButtonRole.less'
import {NavLink} from 'react-router-dom';

const ButtonRole = (props) => {
    const {icon, roleName, route, disabled} = props;

    return (
        <Button
            className="btn-role"
            size="large"
            disabled={disabled}
        >
            <NavLink
                className="btn-role-link"
                to={(route) ? route : "#"}
                exact
            >
                <span>{icon}</span>
                {roleName}
            </NavLink>

        </Button>
    );
};

export default ButtonRole;