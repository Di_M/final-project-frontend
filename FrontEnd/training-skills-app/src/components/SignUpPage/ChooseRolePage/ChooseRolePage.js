import React from 'react';
import ButtonRole from "./Buttons/ButtonRole";
import "./ChooseRolePage.less";
import {routes} from "../../../constants/constRouters";
import { UserAddOutlined, UsergroupDeleteOutlined } from '@ant-design/icons';
import TextWithTransferTo from "../../lib/Navigation/TextWithTransferTo";

const SingUp = () => {

    const iconRoleParent = <UserAddOutlined/>;
    const iconRoleOthers = <UsergroupDeleteOutlined />;

    return (
        <div className="choose-role">

            {/*Content*/}
            <div className="choose-role-content">

                {/*Title block*/}
                <div className="choose-role-title-block">
                    <h2 className="choose-role-title">
                        Let's get you started
                    </h2>
                    <p className="choose-role-description">
                        Choose an account to get you started.
                    </p>
                </div>

                {/*Buttons*/}
                <div className="choose-role-btn-block">
                    <ButtonRole
                        icon={iconRoleParent}
                        roleName="Parent"
                        route={routes.signUpParent.href}
                        disabled={false}
                    />
                    <ButtonRole
                        icon={iconRoleOthers}
                        roleName="Other ..."
                        disabled={true}
                    />
                </div>

                {/*Transfer to ...*/}
                <TextWithTransferTo
                    text="Do you have account? Please "
                    route={routes.signIn.href}
                />
            </div>
        </div>
    );
};

export default SingUp;