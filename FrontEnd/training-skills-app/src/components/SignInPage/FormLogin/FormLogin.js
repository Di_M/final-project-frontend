import React from 'react';
import {
    NavLink,
    useHistory,
} from 'react-router-dom';
import {
    Form,
    Input,
    Button,
    Checkbox,
    Tooltip,
    Select,
    message
} from 'antd';
import {
    UserOutlined,
    LockOutlined,
    InfoCircleOutlined
} from '@ant-design/icons';
import userService from "../../../services/userService";
import { useDispatch } from "react-redux";
import * as TokenActions from '../../../actions/TokenActions';
import * as UserIDActions from '../../../actions/UserIDActions';
import * as RoleActions from '../../../actions/RoleActions';
import { routes } from "../../../constants/constRouters";
import './FormLogin.less';
import TextWithTransferTo from "../../lib/Navigation/TextWithTransferTo";

const { Option } = Select;

const FormLogin = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const onFinish = (values) => {
        console.log('Received values of form: ', values);

        userService.login(values).then((result) => {
            console.log(result);

            if (result.error) {
                console.log(result.error);
                message.error(`${result.error}`);
            } else {
                dispatch(TokenActions.getToken(result.jwtToken));
                dispatch(UserIDActions.getUserID(result.userId));
                dispatch(RoleActions.getRole(values.role));

                (values.role === 'parent')
                    ? history.push(`/account/${result.userId}`)
                    : history.push(`/account/${result.userId}/courses`);
            }
        });
    };

    return (
        <div>
            <Form
                name="normal_login"
                className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Username!',
                        },
                    ]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        placeholder="Name"
                        suffix={
                            <Tooltip title="Login which you have entered ">
                                <InfoCircleOutlined style={{color: 'rgba(0,0,0,.45)'}}/>
                            </Tooltip>}
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>

                <Form.Item name="role" rules={[{required: true}]}>
                    <Select
                        placeholder="Select a role"
                        allowClear
                    >
                        <Option value="parent">parent</Option>
                        <Option value="child">child</Option>
                    </Select>
                </Form.Item>

                <Form.Item>
                    <Form.Item name="remember" noStyle>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <NavLink className="login-form-forgot" to={routes.reestablishPassword.href} exact>Forgot
                        password</NavLink>

                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                </Form.Item>
            </Form>
            <TextWithTransferTo
                text="If you don`t have account? Please "
                route={routes.signUpParent.href}
            />
        </div>
    );
};

export default FormLogin;
