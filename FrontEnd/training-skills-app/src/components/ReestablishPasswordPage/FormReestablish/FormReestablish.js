import React from 'react';
import {Form, Input, Button, Tooltip} from 'antd';
import './FormReestablish.less';
import {InfoCircleOutlined, UserOutlined, MailOutlined} from "@ant-design/icons";

const FormReestablish = () => {

    const onFinish = values => {
        console.log('Received values of form: ', values);
    };

    return (
        <Form
            name="reestablish_password"
            className="reestablish_password"
            onFinish={onFinish}
        >

            <Form.Item
                name="username"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Username!',
                    },
                ]}
            >
                <Input
                    prefix={<UserOutlined className="site-form-item-icon" />}
                    placeholder="Username"
                    suffix={
                        <Tooltip title="Registration Login">
                            <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                        </Tooltip>}
                />
            </Form.Item>

            <Form.Item
                name="email"
                rules={[
                    {
                        type: 'email',
                        message: 'The input is not valid E-mail!',
                    },
                    {
                        required: true,
                        message: 'Please input your E-mail!',
                    },
                ]}
            >
                <Input
                    placeholder="Enter your email"
                    suffix={
                    <Tooltip title="Registration Email">
                        <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                    </Tooltip>}
                />
            </Form.Item>

            <Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                    className="reestablish_password-form-button"
                    icon={<MailOutlined />}
                >
                    Send new password to email
                </Button>
            </Form.Item>
        </Form>
    );
};

export default FormReestablish;