import React from 'react';
import './LessonExampleBlockItem.less'
const uniqid = require('uniqid');

//Component is used in Theory and Question Blocks!

const LessonExampleBlockItem=(props)=>{

    // props must be an example object in a lesson => .exampleBody Array

  const  {id,  imgArray, imgNumber, displayAnswer, description, symbol} = props.lesson

    // get random image from array of exampleImg => added to rowImgArray()

    const randomImg=(array)=>{
        return array[Math.floor(Math.random() * array.length)];
    }

    // generate dynamic row of images

    const rowImgArray = (array, number)=>{
      let arrayImg =[]
      for (let i=0; i<number; i++){
          arrayImg.push( <img src={randomImg(array)} alt="example img" className="lesson-example-img" key={uniqid('img-')}/>)
      }
      return arrayImg
    }
// for TheoryBlock
    const theoryBlock = (
        <React.Fragment>
        <div className="lesson-theory-symbol">{symbol}</div>
    <div className="lesson-theory-type-counting-item-answer">
        <div className="lesson-theory-answer"> <span>{displayAnswer}</span> </div>
    </div>
        </React.Fragment>)


    return (<React.Fragment>

        <div className="lesson-theory-type-counting-item" key={id}>
            <h3 className="lesson-theory-type-counting-item-description">{description}</h3>

            <div className="lesson-theory-type-counting-item-answer-block">

                <div className="lesson-theory-type-counting-item-img">
                    <div>{rowImgArray(imgArray,imgNumber)}</div>
                </div>
                {!props.question?theoryBlock:null}
            </div>
        </div>

    </React.Fragment>)

}

export default LessonExampleBlockItem
