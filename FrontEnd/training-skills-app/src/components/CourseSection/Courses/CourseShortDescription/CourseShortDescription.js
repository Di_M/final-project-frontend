import React from 'react'
import './CourseShortDescription.less'
import {Button, Tooltip} from "antd";
import { QuestionCircleOutlined } from '@ant-design/icons';
import {Link}from "react-router-dom";
import {useSelector} from "react-redux";




const CourseShortDescription = props =>{
    const {courseImg, courseTitle, courseGrade="Elementary", classList = []} = props;
  //  const courseUrl =  `/account/${id}/courses/math';
    let userID= useSelector(store => store.UserIDReducer);
   const containerClasses = ['course-short-description-block', ...classList];

    return <React.Fragment>
        <div className={containerClasses.join(' ')}>
            <div className="course-short-description-title">

                <div className="course-short-description-subject">
                    <img src={courseImg} alt="course img"/>
                    <Link to={`/account/${userID.userID}/courses/math`}><span>{courseTitle}</span></Link>
                </div>
                <div  className="course-short-description-wrapper">
                <div className="course-short-description-grade">{courseGrade} </div>
                <Link to={`/account/${userID.userID}/courses/math`}><Button type="primary"  className="course-short-description-details">See Details</Button></Link>
                </div>

            </div>

            <div className="course-short-description-progress">
                <div>Ready to go...</div>
                <div  className="course-short-description-wrapper">
                    <div className="course-short-description-progress-quiz">Score: <span>0</span> <Tooltip placement="top" title="Your quiz Score"> <QuestionCircleOutlined /></Tooltip></div>
                    <div className="course-short-description-progress-percent">Progress: <span>0</span> <Tooltip placement="top" title="Your progress Status"> <QuestionCircleOutlined /></Tooltip></div>
                </div>
            </div>

        </div>
    </React.Fragment>
}

export default CourseShortDescription

//  <div className="course-short-description-progress-points">Earned points:<span>350</span></div>
