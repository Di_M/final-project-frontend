import React, {useEffect} from 'react'
import fakeCourseData from '../CourseFakeData'
import {Breadcrumb, Collapse} from "antd";
import CourseItem from '../CourseItem/CourseItem'
import './Course.less'
import {Link} from 'react-router-dom'
import QuizComponent from "../../QuizSection/QuizComponent";
import { useSelector } from "react-redux";
const { Panel } = Collapse;




const Course =(props)=>{
//Request Course

        const getCourseDetails  = useSelector(store => store.CourseDetailsReducer);
         const courseData = getCourseDetails.courseDetails

// Display Data fr
    const courseSection_1 = courseData.courseSections[0].courseSectionTitle;
    const courseSection_2 =  courseData.courseSections[1].courseSectionTitle;

    const courseSectionLessons = courseData.courseSections[0].courseSectionLessons;
    const courseSectionLessonsSecondSection = courseData.courseSections[1].courseSectionLessons;



    const headerItemDisplay = (item)=>{

    return ((<h3 className="course-block-section-title">{item}</h3>))
    };


    return (
        <React.Fragment>
            <div className="course-block">

            <div className="course-block-breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item><a href="">Main</a></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="">{courseData.courseTitle}</a>
                </Breadcrumb.Item>
            </Breadcrumb>
            </div>

                <div className="course-block-title"><h3 className="course-block-title-text">{courseData.courseTitle}</h3></div>

                        <div className="course-block-items-wrapper">

                            <div className="course-block-items-header">
                                <div className="course-block-items-header-description"><h3>Description</h3></div>
                                <div className="course-block-items-header-status">
                                    <span>Points</span>
                                    <span>Status</span>
                                </div>
                            </div>

                            <Collapse defaultActiveKey={['1']}>

                                <Panel header={headerItemDisplay(courseSection_1)} key="1" className="course-block-item-panel" >

                                    {courseSectionLessons.map( (item,index)=>{
                                        return(

                                             <CourseItem      key={index}
                                                              lessondbId={item.id}
                                                              lessonId={index}
                                                              lessonImg={item.lessonImg}
                                                              title={item.title}
                                                              lessonPoints={item.lessonPoints}
                                                              lessonIsFinished={item.lessonIsFinished}
                                                              score={false}

                                             />)}

                                    )}


                                </Panel>
                                <Panel header={headerItemDisplay(courseSection_2)}  key="2" className="course-block-item-panel">

                                    {courseSectionLessonsSecondSection.map( (item, index)=>{
                                        let lessonN =10+index;
                                        return(
                                            <CourseItem      key={lessonN}
                                                             lessondbId={item.id}
                                                             lessonId={lessonN}
                                                             lessonImg={item.lessonImg}
                                                             title={item.title}
                                                             lessonPoints={item.lessonPoints}
                                                             lessonIsFinished={item.lessonIsFinished}
                                                             score={false}

                                            />)}

                                    )}


                                </Panel>
                                <Panel header={headerItemDisplay('Final Quiz')} key="3" className="course-block-item-panel">

                                    {/*should be displayed after finished all lessons*/}

                                    <QuizComponent/>
                                </Panel>

                            </Collapse>




                        </div>



            </div>

        </React.Fragment>

    )
}

export default Course
