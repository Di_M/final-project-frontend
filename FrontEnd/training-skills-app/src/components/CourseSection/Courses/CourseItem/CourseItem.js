import React from 'react'
import './CourseItem.less'
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import {EditOutlined, CheckOutlined} from '@ant-design/icons'
import {useSelector} from "react-redux";

const CourseItem =(props)=>{
    let {lessonImg, title, score, lessonPoints, lessonIsFinished,  lessonId, lessondbId} = props

    let userID= useSelector(store => store.UserIDReducer);

    return (
        <React.Fragment>
            <Link to={{
                pathname: `/account/${userID.userID}/courses/math/lesson/${lessonId}`,
            }}>
            <div className="course-item">
                <div className="course-item-title-wrapper">
                    <img src={lessonImg} alt ="img" className="course-item-img"/>
                    <h4>{title}</h4>
                    { score?<div>Score:{score}</div>:null}
                </div>


                <div className="course-item-points-wrapper">
                    <div className="course-item-points">{lessonPoints}</div>
                    {lessonIsFinished?<div className="course-item-points points-finished"> <CheckOutlined /></div>:<div className="course-item-points points-not-finished"><EditOutlined /></div>}
                </div>

            </div>
            </Link>
        </React.Fragment>
    )
}
export default CourseItem