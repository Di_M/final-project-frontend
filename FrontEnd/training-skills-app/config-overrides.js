const { 
    override, 
    fixBabelImports, 
    addLessLoader, 
    addWebpackPlugin 
} = require('customize-cra');
const path = require('path');


const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');



module.exports = override(
    fixBabelImports('antd', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: {
            hack: `true; @import "${path.resolve(
                __dirname,
                'src/components/lib/themes/theme.less'
            )}";`,
        },
    }),
    addWebpackPlugin(new AntdDayjsWebpackPlugin()),
);

  
