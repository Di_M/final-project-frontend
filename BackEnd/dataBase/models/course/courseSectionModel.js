import {Schema, Types, model} from 'mongoose';

require('./lessonModel');

const courseSectionSchema = new Schema({
    courseSectionImg: {
        type: String,
        required: true
    },
    courseSectionId: {
        type: Types.ObjectId,
        required: true
    },
    courseSectionTitle: {
        type: String,
        required: true
    },
    courseSectionDescription: {
        type: String,
        required: true
    },
    courseSectionProgressPercent: {
        type: Number,
        required: true
    },
    courseSectionLessonsFinished: {
        type: Number,
        required: true
    },
    courseSectionEarnedPoints: {
        type: Number,
        required: true
    },
    courseSectionIsFinished: {
        type: Boolean,
        required: true
    },
    courseSectionLessons: [{
        type: Types.ObjectId,
        ref: 'Lesson'
    }] 
});

export default model('Section', courseSectionSchema);
