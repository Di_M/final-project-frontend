import {Schema, Types, model} from 'mongoose';

require('./courseSectionModel');

const courseSchema = new Schema({
    courseImg: {
        type: String,
        required: true,
    },
    courseTitle: {
        type: String,
        required: true,
    },
    courseDescription: {
        type: String,
        required: true,
    },
    courseProgressPercent: {
        type: Number,
    },
    courseIsFinished: {
        type: Boolean,
    },
    courseLessonsFinished: {
        type: Number,
    },
    courseEarnedPoints: {
        type: Number,
    },
    courseSubject: {
      type: String,
      required: true,
    },
    courseGrade: {
        type: String,
        required: true,
    },
    courseSections: [{
        type: Types.ObjectId,
        ref: 'Section'
    }],
    quizSection: {
        quizTitle: { type: String },
        quizSynopsis: { type: String },
        questions: [{
            question: { type: String },
            questionType: { type: String },
            questionPic: { type: String },
            answerSelectionType: { type: String },
            answers: [{ type: String }],
            correctAnswer: { type: String },
            messageForCorrectAnswer: { type: String },
            messageForIncorrectAnswer: { type: String },
            explanation: { type: String },
            point: { type: String },
        }]
    }
});

export default model('Course', courseSchema);
