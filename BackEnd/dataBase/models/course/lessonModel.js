import {Schema, Types, model} from 'mongoose';

export const lessonSchema = new Schema({
    lessonPoints: {
        type: Number,
        required: true,
    },
    lessonImg: {
        type: String,
        required: true,
    },
    lessonType: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    exampleBody: [{
        id: { type: Types.ObjectId },
        imgArray: [{ type: String }],
        imgNumber: { type: Number },
        displayAnswer: { type: Number },
        description: { type: String },
        symbol: { type: String }
    }],
    questionTitle: { type: String },
    questionType: { type: String },
    questionBody: [{
        id: { type: Types.ObjectId },
        imgArray: [{ type: String }],
        imgNumber: { type: Number },
        displayAnswer: { type: String },
        description: { type: String },
        symbol: { type: String },
        question: { type: Boolean },
    }],
    questionAnswer: {
        type: { type: String },
        rightAnswer: { type: Number },
        rightMessage: { type: String },
        IsAnswered: { type: Boolean }
    },
    lessonIsFinished: { type: Boolean }
})

export default model('Lesson', lessonSchema);
