import { Router } from 'express';
import bodyParser from 'body-parser';

import indexRoute from "./routes/indexRoutes";
import signUpRoutes from "./routes/signUpRoutes";
import signInRoutes from "./routes/signInRoutes";
import changePasswordRoutes from "./routes/changePasswordRoutes";
import adminRoutes from "./routes/adminRoutes";
import courseRoutes from "./routes/courseRoutes";
import childRoutes from "./routes/childRoutes";
import lessonRoutes from "./routes/lessonRoutes";

export default () => {
    const app = Router();

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    indexRoute(app);
    signUpRoutes(app);
    signInRoutes(app);
    changePasswordRoutes(app);
    adminRoutes(app);
    courseRoutes(app);
    childRoutes(app);
    lessonRoutes(app);

    return app;
}
