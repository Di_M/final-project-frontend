import { Router, response } from "express";
import verifyToken from "../middleware/token-verification";
import Lesson from "../../dataBase/models/course/lessonModel";
import ChildLesson from "../../dataBase/models/childLessonModel";

const router = Router();

export default app => {
    app.use('/lesson', router);

    router.get('/view/:lessonId',
        verifyToken,
        async (request, response) => {
            const { lessonId } = request.params;
            const { childId } = request.query;

            try {
                const { lessonIsFinished } = await ChildLesson
                    .findOne({
                        childId,
                        lessonId
                    })
                const lesson = await Lesson
                    .findOne(
                        { _id: lessonId },
                        { questionAnswer: 0, lessonIsFinished: 0 }
                    );
                
                response.status(200).json({
                    lesson,
                    lessonIsFinished
                })
            } catch (error) {
                response.status(500).send({message: error});
            }
        }
    )
    router.post('/answer/:lessonId',
        verifyToken,
        async (request, response) => {
            const { lessonId } = request.params;
            const { childId } = request.query;
            const { childAnswer } = request.body;

            try {
                const { questionAnswer, lessonPoints } = await Lesson
                    .findOne(
                        { _id: lessonId },
                        { questionAnswer: 1, lessonPoints: 1 }
                    );
                const { rightAnswer, rightMessage } = questionAnswer;

                if (childAnswer === rightAnswer) {
                    await ChildLesson.updateOne(
                        {
                            childId,
                            lessonId
                        },
                        {
                            $set: {
                                lessonIsFinished: true,
                                lessonEarnedPoints: lessonPoints
                            }
                        }
                    );
                    return response.status(200).send({
                        message: rightMessage
                    })
                } 
                else {
                    return response.status(200).send({
                        message: "Wrong answer, please try again!" // FIXME: specify a messag for a wrong answer
                    })
                }
            } catch (error) {
                response.status(500).send({message: error});
            }
        }
    )
    router.get('/answer/list',verifyToken,
        async (request, response) => {
        try{
            const lessonsList = await Lesson.find()
        } catch(e){
            console.log(e)
        }

        }
        )

}