import dotenv from 'dotenv'

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();

if(!envFound) {
    throw new Error("⚠ Couldn't find .env file ⚠");
}


export default {

    /**
     * Your favorite PORT
     */
    port: parseInt(process.env.PORT, 10),

    /**
     * Your URL to DATA
     */
    databaseURL: process.env.MONGODB_URI,
    /**
     * Your JWT KEY
     */
    jwtKey: process.env.JWT_KEY
}
