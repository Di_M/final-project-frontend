import routes from '../api';
import bodyParser from 'body-parser';

export default ({app}) => {

    // Middleware that transforms the raw string of req.body into json
    app.use(bodyParser.json());

    app.use('/', routes());
}