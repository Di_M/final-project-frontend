import config from '../config';
import mongoose from 'mongoose';

export default async()  => {

    const mongo = await mongoose
        .connect(
          config.databaseURL,
            {
                useNewUrlParser:true,
                useUnifiedTopology:true,
                useCreateIndex:true
            }
        );

    return mongo.connection.db;
}